const electron = require("electron"),
  app = electron.app,
  BrowserWindow = electron.BrowserWindow;
require('electron-debug')({showDevTools: true});

const path = require('path');
const url = require('url');

const fs = require('fs');
const os = require('os');
const ipc = electron.ipcMain;
const shell = electron.shell;

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    autoHideMenuBar: true,
    width:1200,
    height: 700,
    webPreferences: {
      nativeWindowOpen: true
    }
  });
  mainWindow.loadURL(`file://${__dirname}/index.html`);

  mainWindow.webContents.openDevTools();
  mainWindow.on('close', () => {
    mainWindow.webContents.send('stop-server');
  });
  mainWindow.on("closed", () => {
    mainWindow = null;
  });

  
  
}

app.on("ready", createWindow);

app.on("browser-window-created", function (e, window) {
  window.setMenu(null);
});

app.on("window-all-closed", function () {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function () {
  if (mainWindow === null) {
    createWindow();
  }
});

