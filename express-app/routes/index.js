var express = require('express');
var router = express.Router();
var axios = require('axios');
var querystring = require('querystring');
var async = require('async');

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

router.get('/test',function(req,res){
	res.render('test/index',{title:'Test Page'});
});

router.get('/',function(req,res){
	res.render('site/index',{title:'Home Page'});
});

router.get('/login',function(req,res){
	res.render('site/login',{title:'Login Page'});
});

router.post('/login', function(req, res, next){

    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();

    var errors = req.validationErrors();

    if(!errors){

        var dataString = {
            'username': req.body.username,
            'password': req.body.password
        };

        axios({
            method: 'post',
            headers: {"Content-Type":"application/x-www-form-urlencoded"},
            url: 'https://nfc.intrafeed.co/api/login',
            data: querystring.stringify(dataString),
        })
        .then(function (response) {

            if(response.data.error.toString() == "false"){

                var profile = response.data.results.profile;
                var permissions = response.data.results.permissions;

                localStorage.setItem('profile',JSON.stringify(response.data.results));
                req.session.user = profile.username;
                req.session.token = profile.token;
                req.flash('success', 'Berhasil Login');
                res.redirect('/user');

            }else{

            //     localStorage.setItem('profile',[]);

            //     req.flash('error', response.data.message[0].toUpperCase() + response.data.message.slice(1));
            //     res.redirect('/login');
            }

        })
        .catch(function (error) {
            console.log(error);
        });
    }else{
        var error_msg = '';
        errors.forEach(function(error) {
            error_msg += "<p>"+ error.msg + '</p>';
        })

        console.log(error_msg)
        req.flash('error', error_msg);
        res.redirect('/login');
    }

})

module.exports = router;

