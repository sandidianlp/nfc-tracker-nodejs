var express = require('express');
var router = express.Router();
var db = require('./../models/db');
var axios = require('axios');
var querystring = require('querystring');
var async = require('async');
const { URL } = require('url');

var request = require('request');
var fs = require("fs");

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

router.use(function(req,res,next){

	if(req.session.user === null || typeof req.session.user === 'undefined'){
        req.flash('error', 'Anda harus Login terlebih dahulu');
        return res.redirect('/login');
	}

    next();
});

router.get('/',function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	console.log(dataProfile);

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/todo-list',
	}).then(async function (response) {

		var dataList = response.data.results;

		if(dataList != null){

			if(dataList.array != null){
				var printList =  dataList.array;
			}else{
				var printList =  null;
			}

			if(dataList.list != null){
				var guestList =  dataList.list;
			}else{
				var guestList =  null;
			}

		}else{
			var printList,guestList = null;
		}

		res.render('layouts/mainUser',{
			content :'../user/index.ejs',
			title:'User Page',
			data:data,
			printList:printList,
			guestList:guestList
		});



	}).catch(function (error) {
		console.log(error);
	});

});

router.get('/logout',function(req,res){
	async function logout(){
		try {
			localStorage.removeItem('profile');
			req.session.user = null;
    		res.redirect('/login');
		} catch(err){
			console.log(err)
		}

	}
	logout();
});

router.get('/outlet',function(req,res){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/outlet-list',
	}).then(async function (response) {

		res.render('layouts/mainUser',{
			content :'../user/outlet.ejs',
			title:'Data Outlet',
			data:data,
			dataOutlet:response.data.results,
			dataSelect:data.outletId
		});
	}).catch(function (error) {
		console.log(error);
	});
		

});

router.post('/outlet', function(req, res, next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		'outletId': req.body.outlet,
	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/outlet-save',
		data: querystring.stringify(dataString),
	}).then(async function (response) {

		req.flash('success', 'Berhasil Mengubah Outlet');
		res.redirect('/user/outlet');

	}).catch(function (error) {
		console.log(error);
	});

});

router.get('/profile-edit',function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	res.render('layouts/mainUser',{
		content :'../user/profile-edit.ejs',
		title:'Profile User',
		data:data
	});
		
});


router.post('/profile-edit', function(req, res, next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

			var dataString = {
				"name":req.body.name,
				"email":req.body.email,
				"password":req.body.password
			};

			axios({
				method: 'post',
				headers: {
					"Content-Type":"application/x-www-form-urlencoded",
					"Authorization":"Bearer "+data.token
				},
				url: 'https://nfc.intrafeed.co/api/profile',
				data: querystring.stringify(dataString),
			}).then(async function (response) {

				req.flash('success', 'Berhasil Mengubah Password');
				res.redirect('/user/profile-edit');

			}).catch(function (error) {
				console.log(error);
			});

		
});

var multer  =   require('multer');
var storage = multer.diskStorage({
	// destination: function (req, file, callback) {
	// callback(null, './public/uploads');
	// },
	filename: function (req, file, callback) {
		callback(null, file.originalname)
	}
});

var upload = multer({storage: storage}).single('image');

router.post('/create-guest',upload,function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	req.checkBody('nfcId', 'NFC ID is required').notEmpty();
	req.checkBody('name', 'Name is required').notEmpty();
	req.checkBody('email', 'Email is required').notEmpty();
	// req.checkBody("email", "Enter a valid email address.").isEmail();
	req.checkBody('address', 'Address is required').notEmpty();
	req.checkBody('phone', 'Phone is required').notEmpty();

	if(typeof req.file !== "undefined"){
		var image = req.file.filename;
		var msg = 'Please upload an image Jpeg, Png or Gif';
	}else{
		var image = '';
		var msg = 'Image is required';
	}

	req.checkBody('image', msg).isImage(image);

	var errors = req.validationErrors();

	if(!errors){

		var dataString = {
			nfcId:req.body.nfcId,
			name:req.body.name,
			address:req.body.address,
			email:req.body.email,
			phone:req.body.phone,
			image:{
				value:  fs.createReadStream(req.file.path),
				options: {
					filename: req.file.filename,
				}
			}
		};

		request.post({
			url:'https://nfc.intrafeed.co/api/create',
			headers : {
				"Authorization":"Bearer "+data.token
			},
			formData: dataString
		}, function optionalCallback(err, httpResponse, body) {
			if (err) {
			return console.error('upload failed:', err);
			}

			var response  = JSON.parse(body);

			if(response.error === true){
				console.log(response);
				req.flash('error',JSON.stringify(response.message));
				res.redirect('/user/create-guest');
			}else{
				req.flash('success', 'Success save data');
				res.redirect('/user/create-guest');
			}


		});
	}else{

		var open = '<ul>';
		var error_msg = '';
        errors.forEach(function(error) {
            error_msg += '<li>'+error.msg + '</li>';
        })
        var close = '</ul>';

		var message = open+error_msg+close;
	
		req.flash('error', message);
		res.redirect('/user/create-guest');
		
	}

		
});

router.get('/guest-action/:action/:nfcId',function(req,res,next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;


	var dataString = {
		nfcId:req.params.nfcId,
		action:req.params.action
	};
	
	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/guest-action',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		res.send(response.data.results);

		// console.log(response.data);

		// console.log(response.data.error);

		// if(response.data.error === true){
		// 	req.flash('error', response.data.message);
		// 	res.redirect('/user/link-chip');
		// }else{
		// 	req.flash('success', 'Success save new NFC tag');
		// 	res.redirect('/user/link-chip');
		// }

	

	}).catch(function (error) {
		console.log(error);
	});

		
});

router.get('/scan',function(req,res,next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	res.render('layouts/mainUser',{
		content :'../user/scan.ejs',
		title:'Scan NFC Page',
		data:data
	});

		
});


router.post('/receipt', function(req, res, next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		"nfcId":req.body.nfcId,
		"receipt":req.body.receipt

	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/receipt',
		data: querystring.stringify(dataString),
	}).then(async function (response) {

		var guest = response.data.results;
		
		req.flash('success', 'Success Access');
		res.redirect('/user/scan');

	}).catch(function (error) {
		console.log(error);
	});

		
});

router.get('/guest/:id',function(req,res,next){
	// var dataProfile = JSON.parse(localStorage.getItem('profile'));
	// var data = dataProfile.profile;

	// var dataGuest = await db.get("SELECT * FROM guest WHERE nfcid = ?" ,[req.params.id]);

	// axios({
	// 	method: 'post',
	// 	headers: {
	// 		"Content-Type":"application/x-www-form-urlencoded",
	// 		"Authorization":"Bearer "+data.token
	// 	},
	// 	url: 'https://nfc.intrafeed.co/api/outlet-list',
	// }).then(async function (response) {

	// 	var outlet = response.data.results[data.outletId];

	// 	res.render('layouts/mainUser',{
	// 		content :'../user/guest.ejs',
	// 		title:'Guest Page',
	// 		data:data,
	// 		dataGuest:dataGuest,
	// 		outlet:outlet

	// 	});

	// }).catch(function (error) {
	// 	console.log(error);
	// });


		
});

router.get('/create-guest',function(req,res,next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	// var data = await db.get("SELECT * FROM users WHERE username = ?" ,[req.session.user]);

	res.render('layouts/mainUser',{
		content :'../user/create-guest.ejs',
		title:'Create Guest Page',
		data:data,
		input:''
	});

		
});


// var multer  =   require('multer');
// var storage = multer.diskStorage({
// 	// destination: function (req, file, callback) {
// 	// callback(null, './public/uploads');
// 	// },
// 	filename: function (req, file, callback) {
// 		callback(null, Date.now()+'-'+file.originalname)
// 	}
// });

// var upload = multer({storage: storage}).single('image');

// router.post('/create-guest',upload,function(req,res,next){

// 	var dataProfile = JSON.parse(localStorage.getItem('profile'));
// 	var data = dataProfile.profile;


// 	var dataString = {
// 		nfcId:req.body.nfcId,
// 		name:req.body.name,
// 		address:req.body.address,
// 		email:req.body.email,
// 		phone:req.body.phone,
// 		image: {
// 			value:  fs.createReadStream(req.file.path),
// 			options: {
// 				filename: req.file.filename,
// 			}
// 		}
// 	};

// 	// console.log(dataString);

// 	request.post({
// 		url:'https://nfc.intrafeed.co/api/create',
// 		headers : {
// 			"Authorization":"Bearer "+data.token
// 		},
// 		formData: dataString
// 	}, function optionalCallback(err, httpResponse, body) {
// 		if (err) {
// 		return console.error('upload failed:', err);
// 		}
// 		console.log(body);
// 	});


	
// });

router.get('/link-chip',function(req,res,next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	res.render('layouts/mainUser',{
		content :'../user/link-chip.ejs',
		title:'Link Chip With Guest',
		data:data,
		data
	});

		
});

router.get('/save-chip/:nfcId/:guestId',function(req,res,next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		nfcId:req.params.nfcId,
		guestId:req.params.guestId
	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/linked',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		console.log(response.data.error);

		if(response.data.error === true){
			req.flash('error', response.data.message);
			res.redirect('/user/link-chip');
		}else{
			req.flash('success', 'Success save new NFC tag');
			res.redirect('/user/link-chip');
		}


	}).catch(function (error) {
		console.log(error);
	});

		
});

router.get('/lookup/:nfcId',function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	res.render('layouts/mainUser',{
		content :'../user/lookup.ejs',
		title:'Look Up Page',
		data:data,
		results:'',
		input:'',
		get:req.params
	});

		
});

router.post('/lookup/:nfcId',function(req,res,next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		membershipId:req.body.membershipId,
		email:req.body.email,
		phone:req.body.phone
	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/lookup',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		console.log(response.data.results);

		res.render('layouts/mainUser',{
			content :'../user/lookup.ejs',
			title:'Create Guest Page',
			data:data,
			results:response.data.results,
			input:dataString,
			get:req.params
		});

	}).catch(function (error) {
		console.log(error);
	});

		
});


router.get('/detail-guest/:id',function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		id:req.params.id,
		
	};
	// console.log(dataString);

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/guest',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		console.log(response.data.results);

		res.render('layouts/mainUser',{
			content :'../user/detail-guest.ejs',
			title:'Detail Guest Page',
			data:data,
			results:response.data.results,
		});

	}).catch(function (error) {
		console.log(error);
	});

		
});

router.get('/look',function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	res.render('layouts/mainUser',{
		content :'../user/look.ejs',
		title:'Look Up Page',
		data:data,
		results:'',
		input:'',
	});

		
});

router.post('/look',function(req,res,next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		membershipId:req.body.membershipId,
		email:req.body.email,
		phone:req.body.phone,
		name:req.body.name
	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/lookup',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		console.log(response.data.results);

		res.render('layouts/mainUser',{
			content :'../user/look.ejs',
			title:'Create Guest Page',
			data:data,
			results:response.data.results,
			input:dataString,
		});

	}).catch(function (error) {
		console.log(error);
	});

		
});


router.get('/linked/:nfcId/:guestId',function(req,res,next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		guestId:req.params.guestId
	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/linked',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		// console.log(response.data.results);

		res.render('layouts/mainUser',{
			content :'../user/linked.ejs',
			title:'Linked Guest Page',
			data:data,
			results:response.data.results,
		});

	}).catch(function (error) {
		console.log(error);
	});

});

router.get("/update/:id",function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		id:req.params.id
	}

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/edit',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		console.log(response.data);
		// // if(response.data.error.toString() == "false"){
			res.render('layouts/mainUser',{
				content :'../user/update.ejs',
				title:'Update Guest Page',
				data:data,
				results:response.data.results,
			});
		// // }else{
			
		// // }
	}).catch(function (error) {
		console.log(error);
	});
})


router.post('/update', function(req, res, next){
	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	var dataString = {
		name:req.body.name,
		name:req.body.name,
		id:req.body.id
		
	};

	console.log(dataString)
})

router.get('/print-guest/:id',function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	// var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

	// const myURL = new URL(fullUrl);

	var dataString = {
		// id:myURL.searchParams.get('id')
		id:req.params.id
	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/print-guest',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		console.log(response.data.results);

		// res.attachment();
		res.render('user/print-guest.ejs',{
			results:response.data.results
		});


	}).catch(function (error) {
		console.log(error);
	});

});

router.get('/print-guest2/:id',function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	// var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

	// const myURL = new URL(fullUrl);

	var dataString = {
		// id:myURL.searchParams.get('id')
		id:req.params.id
	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/print-guest',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		console.log(response.data.results);

		// res.attachment();
		res.render('user/print-guest.ejs',{
			results:response.data.results
		});


	}).catch(function (error) {
		console.log(error);
	});

});

router.get('/print/:id',function(req,res,next){

	var dataProfile = JSON.parse(localStorage.getItem('profile'));
	var data = dataProfile.profile;

	// var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

	// const myURL = new URL(fullUrl);

	var dataString = {
		// id:myURL.searchParams.get('id')
		id:req.params.id
	};

	axios({
		method: 'post',
		headers: {
			"Content-Type":"application/x-www-form-urlencoded",
			"Authorization":"Bearer "+data.token
		},
		url: 'https://nfc.intrafeed.co/api/print',
		data: querystring.stringify(dataString),
	}).then(function (response) {

		console.log(response.data.results);

		// res.attachment();
		res.render('user/print.ejs',{
			results:response.data.results
		});


	}).catch(function (error) {
		console.log(error);
	});

});

// router.get('/print-guest',function(req,res,next){

// 	var dataProfile = JSON.parse(localStorage.getItem('profile'));
// 	var data = dataProfile.profile;

// 	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

// 	const myURL = new URL(fullUrl);

// 	var dataString = {
// 		id:myURL.searchParams.get('id')
// 	};
	
// 	axios({
// 		method: 'post',
// 		headers: {
// 			"Content-Type":"application/x-www-form-urlencoded",
// 			"Authorization":"Bearer "+data.token
// 		},
// 		url: 'https://nfc.intrafeed.co/api/print-guest',
// 		data: querystring.stringify(dataString),
// 	}).then(function (response) {

// 		console.log(response.data.results);

// 		res.attachment();
// 		res.render('user/print-guest.ejs',{
// 			results:response.data.results
// 		});

	
// 	}).catch(function (error) {
// 		console.log(error);
// 	});

// });

module.exports = router;

