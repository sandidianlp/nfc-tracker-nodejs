
var express = require('express');
var validator = require('express-validator');
var app = express();
var request = require("request");
var path = require('path');
var querystring = require('querystring');
var async = require('async');
var flash = require('express-flash')

//view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");


var index = require('./routes/index');
var user = require('./routes/user');

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(flash());

app.use(validator({
customValidators: {
    isImage: function(value, filename) {

        var extension = (path.extname(filename)).toLowerCase();
        switch (extension) {
            case '.jpg':
                return '.jpg';
            case '.jpeg':
                return '.jpeg';
            case  '.png':
                return '.png';
            default:
                return false;
        }
    }
}}));

app.use(express.static(path.join(__dirname, 'public')));

var cookieParser = require('cookie-parser');
var session = require('express-session');

app.use(cookieParser('keyboard cat'))
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 6000000 }
}))

app.use(function(req, res, next){
    res.locals.user = "";
    res.locals.session= req.session;
    if(req.session.user){
        res.locals.user= req.session.user;
    }
    next();
});


app.use('/', index);
app.use('/user', user);

module.exports=app;
