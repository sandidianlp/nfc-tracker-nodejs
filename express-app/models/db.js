var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('nfc-usb.db');

exports.connect=db;

exports.run=function(query,params){
    return new Promise(function(resolve){
        db.run(query,params,function(err) {
            if(err){
                reject(err.message)
            }else{
                resolve(true)
            }
        })
    })
}

exports.get=function(query,params){
    return new Promise(function(resolve,reject){
        db.get(query,params,function(err,row) {
            if(err){
                reject("Read error: " + err.message)
            }else {
                resolve(row)
            }
        })
    })
}

